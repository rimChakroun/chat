//L'application requiert l'utition d'express pour gerer les routes
const express = require("express");
//L'application requiert l'utilisation de mysql pour acceder à la BD
const mysql = require("mysql"); //passer les contenu les données envoyées (formulaire)
const bodyParser = require("body-parser");
// Les paramètres du serveur.
var hostname = "localhost";
var port = 3000;
//creation d'un objet express=== serveur
var app = express();
app.get("/", function(req, res) {
  var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "admin",
    database: "chat"
  });
  connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });
  res.send("Bonjour");
});

// mettre mon serveur en ecoute
app.listen(port, hostname, function() {
  console.log("Mon serveur API fonctionne sur http://" + hostname + ":" + port);
});
