var mysql = require("mysql");

console.log("Get connection ...");

var conn = mysql.createConnection({
  database: "chat",
  host: "localhost",
  user: "root",
  password: "password"
});

conn.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
conn.query("SELECT * from users", function(err, rows, fields) {
  if (!err) console.log("The solution is: ", rows);
  else console.log("Error while performing Query.");
});
